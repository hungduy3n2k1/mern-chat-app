const createError = require("http-errors");
const messageModel = require("../models/messageModel");
const userModel = require("../models/userModel");

const sendMessage = async (data) => {
  try {
    var message = await messageModel.create(data);

    message = await message.populate("sender", "name avatar");
    message = await message.populate("chat");
    message = await userModel.populate(message, {
      path: "chat.users",
      select: "name avatar email",
    });

    return message;
  } catch (error) {
    throw createError(500, error);
  }
};

const allMessages = async (req) => {
  try {
    const messages = await messageModel
      .find({ chat: req.params.chatId })
      .populate("sender", "name avatar email")
      .populate("chat");

    return messages;
  } catch (error) {
    throw createError(500, error);
  }
};

module.exports = { sendMessage, allMessages };
