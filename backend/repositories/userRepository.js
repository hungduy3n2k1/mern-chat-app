const createError = require("http-errors");
const UserModel = require("../models/userModel");

const registerUser = async (req) => {
  try {
    const { name, email, password, avatar } = req.body;

    const newUser = await UserModel.create({
      name,
      email,
      password,
      avatar,
    });

    return newUser;
  } catch (error) {
    throw createError(500, error);
  }
};

const getUserByEmail = async (email) => {
  try {
    const user = await UserModel.findOne({ email });

    return user;
  } catch (error) {
    throw createError(500, error);
  }
};

const getUserByEmailOrName = async (req) => {
  try {
    const keyword = req.query.search
      ? {
          $or: [
            { name: { $regex: req.query.search, $options: "i" } },
            { email: { $regex: req.query.search, $options: "i" } },
          ],
        }
      : {};

    const users = await UserModel.find(keyword).find({
      _id: { $ne: req.user._id },
    });

    return users;
  } catch (error) {
    throw createError(500, error);
  }
};

module.exports = {
  registerUser,
  getUserByEmail,
  getUserByEmailOrName,
};
