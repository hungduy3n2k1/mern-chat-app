const createError = require("http-errors");
const chatModel = require("../models/chatModel");
const userModel = require("../models/userModel");

const isChat = async (req) => {
  try {
    const { userId } = req.body;

    var isChat = await chatModel
      .find({
        isGroup: false,
        $and: [
          { users: { $elemMatch: { $eq: req.user._id } } },
          { users: { $elemMatch: { $eq: userId } } },
        ],
      })
      .populate("users", "-password")
      .populate("latestMessage");

    isChat = await userModel.populate(isChat, {
      path: "latestMessage.sender",
      select: "name avatar email",
    });

    return isChat;
  } catch (error) {
    throw createError(500, error);
  }
};

const createChat = async (data) => {
  try {
    const newChat = await chatModel.create(data);

    return newChat;
  } catch (error) {
    throw createError(500, error);
  }
};

const getChatById = async (chatId) => {
  try {
    const chatDetail = await chatModel
      .findOne({
        _id: chatId,
      })
      .populate("users", "-password");

    return chatDetail;
  } catch (error) {
    throw createError(500, error);
  }
};

const fetchChats = async (req) => {
  try {
    const allChats = await chatModel
      .find({
        users: {
          $elemMatch: {
            $eq: req.user._id,
          },
        },
      })
      .populate("users", "-password")
      .populate("groupAdmin", "-password")
      .populate("latestMessage")
      .sort({ updatedAt: -1 })
      .then(async (results) => {
        results = await userModel.populate(results, {
          path: "latestMessage.sender",
          select: "name avatar email",
        });
        return results;
      });

    return allChats;
  } catch (error) {
    throw createError(500, error);
  }
};

const createGroup = async (req, users) => {
  try {
    const newGroup = await chatModel.create({
      chatName: req.body.name,
      users: users,
      isGroup: true,
      groupAdmin: req.user,
    });

    return newGroup;
  } catch (error) {
    throw createError(500, error);
  }
};

const getGroupById = async (groupId) => {
  try {
    const groupDetail = await chatModel
      .findOne({
        _id: groupId,
      })
      .populate("users", "-password")
      .populate("groupAdmin", "-password");

    return groupDetail;
  } catch (error) {
    throw createError(500, error);
  }
};

const renameGroup = async (req) => {
  try {
    const { chatId, chatName } = req.body;

    const renamedGroup = await chatModel
      .findByIdAndUpdate(
        chatId,
        {
          chatName: chatName,
        },
        {
          new: true,
        }
      )
      .populate("users", "-password")
      .populate("groupAdmin", "-password");

    return renamedGroup;
  } catch (error) {
    throw createError(500, error);
  }
};

const addToGroup = async (req) => {
  try {
    const { chatId, userId } = req.body;

    const updatedGroup = await chatModel
      .findByIdAndUpdate(
        chatId,
        {
          $push: { users: userId },
        },
        {
          new: true,
        }
      )
      .populate("users", "-password")
      .populate("groupAdmin", "-password");

    return updatedGroup;
  } catch (error) {
    throw createError(500, error);
  }
};

const removeFromGroup = async (req) => {
  try {
    const { chatId, userId } = req.body;

    const updatedGroup = await chatModel
      .findByIdAndUpdate(
        chatId,
        {
          $pull: { users: userId },
        },
        {
          new: true,
        }
      )
      .populate("users", "-password")
      .populate("groupAdmin", "-password");

    return updatedGroup;
  } catch (error) {
    throw createError(500, error);
  }
};

const findByIdAndUpdate = async (chatId, data) => {
  try {
    const result = await chatModel.findByIdAndUpdate(chatId, { ...data });

    return result;
  } catch (error) {
    throw createError(500, error);
  }
};

module.exports = {
  isChat,
  getChatById,
  createChat,
  fetchChats,
  createGroup,
  getGroupById,
  renameGroup,
  addToGroup,
  removeFromGroup,
  findByIdAndUpdate,
};
