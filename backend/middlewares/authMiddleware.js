const jwt = require("jsonwebtoken");
const createError = require("http-errors");
const userModel = require("../models/userModel");
const asyncHandler = require("express-async-handler");

const authMiddleware = asyncHandler(async (req, res, next) => {
  let token;

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    try {
      token = req.headers.authorization.split(" ")[1];

      //decodes token id
      const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);

      req.user = await userModel.findById(decoded.id).select("-password");

      next();
    } catch (error) {
      throw createError(401, "Not authorized, token failed");
    }
  }

  if (!token) {
    throw createError(401, "Not authorized, no token");
  }
});

module.exports = authMiddleware;
