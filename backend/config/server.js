const corsOptions = {
    origin: "*", // Cho phép truy cập từ tất cả các nguồn
    credentials: true, // Cho phép gửi cookie từ trình duyệt của khách hàng
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // Các phương thức HTTP được cho phép
    preflightContinue: false,
    optionsSuccessStatus: 204
};

const limiter = (rateLimit) => {
    return rateLimit({
        windowMs: 60 * 1000,
        max:120,
        standardHeaders: true,
        keyGenerator: (req, res) => req.ip,
        handler: (req, res) => {
            res.status(429).json({
                message: 'Too many requests, please try again later.'
            });
        }
    })
};

module.exports = {
    corsOptions,
    limiter
}