const createError = require("http-errors");
const messageRepository = require("../repositories/messageRepository");
const chatRepository = require("../repositories/chatRepository");

const sendMessage = async (req) => {
  const { content, chatId } = req.body;

  if (!content || !chatId) {
    throw createError(400, "Invalid data passed into request");
  }

  try {
    var newMessage = {
      sender: req.user._id,
      content: content,
      chat: chatId,
    };

    const message = await messageRepository.sendMessage(newMessage);

    if (!message) {
      throw createError(500, "Failed to send message");
    }

    const result = await chatRepository.findByIdAndUpdate(chatId, {
      latestMessage: message,
    });

    if (!result) {
      throw createError(500, "Failed to update chat");
    }

    return message;
  } catch (error) {
    throw error;
  }
};

const allMessages = async (req) => {
  try {
    const messages = await messageRepository.allMessages(req);

    return messages;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  sendMessage,
  allMessages,
};
