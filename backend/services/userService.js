const jwt = require("jsonwebtoken");
const createError = require("http-errors");
const userRepository = require("../repositories/userRepository");

const registerUser = async (req) => {
  const { name, email, password, avatar } = req.body;

  if (!name || !email || !password) {
    throw createError(400, "Please enter all the fields");
  }

  try {
    const userExists = await userRepository.getUserByEmail(email);
    if (userExists) {
      throw createError(400, "User already exists");
    }
  } catch (error) {
    throw error;
  }

  try {
    let newUser = await userRepository.registerUser(req);
    if (!newUser) {
      throw createError(500, "Failed to create new data");
    }

    newUser = newUser.toObject();

    const accessToken = generateToken(newUser);
    return {
      ...newUser,
      access_token: accessToken,
    };
  } catch (error) {
    throw error;
  }
};

const loginUser = async (req) => {
  const { email, password } = req.body;

  if (!email || !password) {
    throw createError(400, "Please enter all the fields");
  }

  try {
    let userExists = await userRepository.getUserByEmail(email);
    if (!userExists) {
      throw createError(401, "Email or password is incorrect");
    }

    if (await userExists.matchPassword(password)) {
      userExists = userExists.toObject();
      const accessToken = generateToken(userExists);
      return {
        ...userExists,
        access_token: accessToken,
      };
    }
  } catch (error) {
    throw error;
  }
};

const allUsers = async (req) => {
  try {
    let users = await userRepository.getUserByEmailOrName(req);
    return users;
  } catch (error) {
    throw error;
  }
};

const generateToken = (user) => {
  const token = jwt.sign(
    {
      id: user._id,
      name: user.name,
      email: user.email,
    },
    process.env.JWT_SECRET_KEY,
    { expiresIn: "24h" }
  );

  return token;
};

module.exports = {
  loginUser,
  registerUser,
  allUsers,
};
