const createError = require("http-errors");
const chatRepository = require("../repositories/chatRepository");

const accessChat = async (req) => {
  const { userId } = req.body;

  if (!userId) {
    throw createError(400, "UserId params not sent with request");
  }

  try {
    let isChat = await chatRepository.isChat(req);

    if (isChat.length > 0) {
      return isChat[0];
    } else {
      var chatData = {
        chatName: "sender",
        isGroup: false,
        users: [req.user._id, userId],
      };

      try {
        const newChat = await chatRepository.createChat(chatData);

        if (!newChat) {
          throw createError(500, "Failed to create new data");
        }

        const fullChat = await chatRepository.getChatById(newChat._id);

        return fullChat;
      } catch (error) {
        throw error;
      }
    }
  } catch (error) {
    throw error;
  }
};

const fetchChats = async (req) => {
  try {
    const allChats = await chatRepository.fetchChats(req);

    return allChats;
  } catch (error) {
    throw error;
  }
};

const createGroup = async (req) => {
  try {
    if (!req.body.users || !req.body.name) {
      throw createError(400, "Please fill all the fields");
    }

    var users = JSON.parse(req.body.users);

    if (users.length < 2) {
      throw createError(400, "More than 2 users are required to from a group");
    }

    users.push(req.user);

    const newGroup = await chatRepository.createGroup(req, users);

    const fullGroup = await chatRepository.getGroupById(newGroup._id);

    return fullGroup;
  } catch (error) {
    throw error;
  }
};

const renameGroup = async (req) => {
  try {
    const renamedGroup = await chatRepository.renameGroup(req);

    return renamedGroup;
  } catch (error) {
    throw error;
  }
};

const addToGroup = async (req) => {
  try {
    const renamedGroup = await chatRepository.addToGroup(req);

    return renamedGroup;
  } catch (error) {
    throw error;
  }
};

const removeFromGroup = async (req) => {
  try {
    const renamedGroup = await chatRepository.removeFromGroup(req);

    return renamedGroup;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  accessChat,
  fetchChats,
  createGroup,
  renameGroup,
  addToGroup,
  removeFromGroup,
};
