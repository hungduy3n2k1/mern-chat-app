const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
const multer = require("multer");
const colors = require("colors");
const { chats } = require("./data/data");
const { corsOptions } = require("./config/server");
const { MongoClient } = require("mongodb");
const connectDB = require("./config/database");
const routes = require("./routes/index");
const { errorHandler, notFound } = require("./middlewares/errorMiddleware");
dotenv.config();

connectDB();

const app = express();

const upload = multer();

app.use(cors(corsOptions));
app.use(express.json({}));
app.use(express.urlencoded({ extended: true }));
app.use(upload.none());

app.get("/", (req, res) => {
  res.send("API is running success");
});

app.get("/api/chats", (req, res) => {
  res.send(chats);
});

app.use(routes);

app.use(notFound);
app.use(errorHandler);

const PORT = process.env.PORT || 3000;
const server = app.listen(
  PORT,
  console.log(`Server start on port ${PORT}`.green)
);

const io = require("socket.io")(server, {
  pingTimeout: 60000,
  cors: {
    origin: "http://localhost:3000",
  },
});

const onlineUsers = new Set();

io.on("connection", (socket) => {
  console.log("Connected to socket.io");

  socket.on("setup", (userData) => {
    socket.join(userData._id);
    socket.emit("connected");
  });

  socket.on("join chat", (room) => {
    socket.join(room);
    console.log("User Joined Room: " + room);
  });

  socket.on("typing", (room) => socket.in(room).emit("typing"));
  socket.on("stop typing", (room) => socket.in(room).emit("stop typing"));

  socket.on("new message", (newMessageRecieved) => {
    var chat = newMessageRecieved.chat;

    if (!chat.users) return console.log("chat.users not defined");

    chat.users.forEach((user) => {
      if (user._id == newMessageRecieved.sender._id) return;

      socket.in(user._id).emit("message recieved", newMessageRecieved);
    });
  });

  socket.off("setup", () => {
    console.log("User disconnected");
    socket.leave(userData._id);
  });
});
