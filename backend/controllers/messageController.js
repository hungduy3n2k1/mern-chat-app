const asyncHandler = require("express-async-handler");
const colors = require("colors");
const messageService = require("../services/messageService");

const sendMessage = asyncHandler(async (req, res) => {
  try {
    const result = await messageService.sendMessage(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to send message: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

const allMessages = asyncHandler(async (req, res) => {
  try {
    const result = await messageService.allMessages(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to get all messages: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

module.exports = {
  sendMessage,
  allMessages,
};
