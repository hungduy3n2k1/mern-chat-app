const asyncHandler = require("express-async-handler");
const colors = require("colors");
const chatService = require("../services/chatService");

const accessChat = asyncHandler(async (req, res) => {
  try {
    const result = await chatService.accessChat(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to access chat: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

const fetchChats = asyncHandler(async (req, res) => {
  try {
    const result = await chatService.fetchChats(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to fetch chats: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

const createGroup = asyncHandler(async (req, res) => {
  try {
    const result = await chatService.createGroup(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to create group: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

const renameGroup = asyncHandler(async (req, res) => {
  try {
    const result = await chatService.renameGroup(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to rename group: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

const addToGroup = asyncHandler(async (req, res) => {
  try {
    const result = await chatService.addToGroup(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to add to group: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

const removeFromGroup = asyncHandler(async (req, res) => {
  try {
    const result = await chatService.removeFromGroup(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to remove from group: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

module.exports = {
  accessChat,
  fetchChats,
  createGroup,
  renameGroup,
  addToGroup,
  removeFromGroup,
};
