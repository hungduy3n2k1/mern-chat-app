const asyncHandler = require("express-async-handler");
const colors = require("colors");
const userService = require("../services/userService");

const registerUser = asyncHandler(async (req, res) => {
  try {
    const result = await userService.registerUser(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to register user: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

const loginUser = asyncHandler(async (req, res) => {
  try {
    const result = await userService.loginUser(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to login user: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

const allUsers = asyncHandler(async (req, res) => {
  try {
    const result = await userService.allUsers(req);
    res.json(result);
  } catch (error) {
    console.error(`Failed to get all users: ${error.message}`.red.bold);
    const status = error.statusCode || 500;
    return res.status(status).json({ message: error.message });
  }
});

module.exports = {
  registerUser,
  loginUser,
  allUsers,
};
