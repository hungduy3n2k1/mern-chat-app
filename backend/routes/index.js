const express = require("express");
const userRoutes = require("../routes/userRoutes");
const chatRoutes = require("./chatRoutes");
const messageRoutes = require("./messageRoutes");

const router = express.Router();

router.use("/api/user", userRoutes);
router.use("/api/chat", chatRoutes);
router.use("/api/message", messageRoutes);

module.exports = router;
