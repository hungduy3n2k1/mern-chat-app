const express = require("express");
const chatController = require("../controllers/chatController");
const authMiddleware = require("../middlewares/authMiddleware");

const router = express.Router();

router.route("/").post(authMiddleware, chatController.accessChat);
router.route("/").get(authMiddleware, chatController.fetchChats);
router.route("/group").post(authMiddleware, chatController.createGroup);
router.route("/rename").put(authMiddleware, chatController.renameGroup);
router.route("/groupadd").put(authMiddleware, chatController.addToGroup);

router
  .route("/groupremove")
  .put(authMiddleware, chatController.removeFromGroup);

module.exports = router;
