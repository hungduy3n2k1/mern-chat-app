const express = require("express");
const messageController = require("../controllers/messageController");
const authMiddleware = require("../middlewares/authMiddleware");

const router = express.Router();

router.route("/").post(authMiddleware, messageController.sendMessage);
router.route("/:chatId").get(authMiddleware, messageController.allMessages);

module.exports = router;
