import { Box } from "@chakra-ui/react";
import SideDrawer from "../components/layout/SideDrawer";
import { ChatState } from "../context/ChatProvider";
import MyChat from "../components/layout/MyChat";
import ChatBox from "../components/layout/ChatBox";
import { useState } from "react";

const Chat = () => {
  const [fetchAgain, setFetchAgain] = useState(false);

  const { user } = ChatState();

  return (
    <div style={{ width: "100%" }}>
      {user && <SideDrawer />}
      <Box
        display="flex"
        justifyContent="space-between"
        w="100%"
        h="95.3vh"
        // p="10px"
      >
        {user && <MyChat fetchAgain={fetchAgain} />}
        {user && (
          <ChatBox fetchAgain={fetchAgain} setFetchAgain={setFetchAgain} />
        )}
      </Box>
    </div>
  );
};

export default Chat;
