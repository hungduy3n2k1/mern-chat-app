import React, { useEffect, useRef, useState } from "react";
import { ChatState } from "../../context/ChatProvider";
import {
  Box,
  FormControl,
  IconButton,
  Input,
  Spinner,
  Text,
  useToast,
  Avatar,
  Textarea,
} from "@chakra-ui/react";
import axios from "axios";
import { ArrowBackIcon } from "@chakra-ui/icons";
import { getAvatar, getSender, getSenderFull } from "../../config/ChatConfig";
import ProfileModal from "../modal/ProfileModal";
import UpdateGroupChatModal from "../modal/UpdateGroupChatModal";
import "../../assets/singleChat.css";
import ScrollableChat from "./ScrollableChat";
import { trim } from "lodash";

import io from "socket.io-client";
const ENDPOINT = "http://localhost:8080";
var socket, selectedChatCompare;

const SingleChat = ({ fetchAgain, setFetchAgain }) => {
  const [messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(false);
  const [newMessage, setNewMessage] = useState("");
  const [socketConnected, setSocketConnected] = useState(false);
  const [typing, setTyping] = useState(false);
  const [istyping, setIsTyping] = useState(false);
  const toast = useToast();
  const textareaRef = useRef(null);

  const { user, selectedChat, setSelectedChat, notification, setNotification } =
    ChatState();

  useEffect(() => {
    socket = io(ENDPOINT);
    socket.emit("setup", user);
    socket.on("connected", () => setSocketConnected(true));
    socket.on("typing", () => setIsTyping(true));
    socket.on("stop typing", () => setIsTyping(false));
    // eslint - disable - next - line;
  }, []);

  useEffect(() => {
    fetchMessages();

    selectedChatCompare = selectedChat;
    // eslint-disable-next-line
  }, [selectedChat]);

  useEffect(() => {
    socket.on("message recieved", (newMessageRecieved) => {
      if (
        !selectedChatCompare || // if chat is not selected or doesn't match current chat
        selectedChatCompare._id !== newMessageRecieved.chat._id
      ) {
        if (!notification.includes(newMessageRecieved)) {
          setNotification([newMessageRecieved, ...notification]);
        }
      } else {
        setMessages([...messages, newMessageRecieved]);
      }
      setFetchAgain(!fetchAgain);
      setSelectedChat(selectedChatCompare);
    });
  });

  const fetchMessages = async () => {
    if (!selectedChat) return;

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${user.access_token}`,
        },
      };

      setLoading(true);

      const { data } = await axios.get(
        `/api/message/${selectedChat._id}`,
        config
      );
      setMessages(data);
      setLoading(false);

      socket.emit("join chat", selectedChat._id);
    } catch (error) {
      toast({
        title: "Error Occured!",
        description: "Failed to Load the Messages",
        status: "error",
        duration: 5000,
        isClosable: true,
        position: "bottom",
      });
    }
  };

  const sendMessage = async (event) => {
    if (event.key === "Enter" && !event.shiftKey) {
      event.preventDefault();
      // Kiểm tra nếu có dữ liệu trong <Textarea>, thực hiện submit form ở đây
      if (newMessage.trim() !== "") {
        socket.emit("stop typing", selectedChat._id);
        try {
          const config = {
            headers: {
              "Content-type": "application/json",
              Authorization: `Bearer ${user.access_token}`,
            },
          };
          setNewMessage("");
          const { data } = await axios.post(
            "/api/message",
            {
              content: newMessage,
              chatId: selectedChat,
            },
            config
          );
          socket.emit("new message", data);
          setMessages([...messages, data]);
        } catch (error) {
          toast({
            title: "Error Occured!",
            description: "Failed to send the Message",
            status: "error",
            duration: 5000,
            isClosable: true,
            position: "bottom",
          });
        }
      }
    } else if (event.shiftKey && event.key === "Enter") {
      const textArea = textareaRef.current;

      // Ngăn mặc định hành vi của việc chèn dòng mới
      event.preventDefault();

      // Chèn ký tự xuống dòng tại vị trí con trỏ hiện tại
      const { selectionStart, selectionEnd, value } = textArea;
      const newValue =
        value.slice(0, selectionStart) + "\n" + value.slice(selectionEnd);
      textArea.value = newValue;

      setNewMessage(newValue);

      // Di chuyển con trỏ đến vị trí phù hợp sau ký tự xuống dòng
      textArea.selectionStart = textArea.selectionEnd = selectionStart + 1;
    }
    adjustTextareaHeight();
  };

  const typingHandler = (e) => {
    setNewMessage(e.target.value);

    adjustTextareaHeight();

    if (!socketConnected) return;

    if (!typing) {
      setTyping(true);
      socket.emit("typing", selectedChat._id);
    }
    let lastTypingTime = new Date().getTime();
    var timerLength = 3000;
    setTimeout(() => {
      var timeNow = new Date().getTime();
      var timeDiff = timeNow - lastTypingTime;
      if (timeDiff >= timerLength && typing) {
        socket.emit("stop typing", selectedChat._id);
        setTyping(false);
      }
    }, timerLength);
  };

  var defaultMinHeight = 20;

  const adjustTextareaHeight = () => {
    const textarea = textareaRef.current;
    if (textarea) {
      const maxAllowedHeight = 200; // Giới hạn chiều cao tối đa (điều chỉnh giá trị tùy ý)

      textarea.style.height = "auto"; // Đặt lại chiều cao thành tự động
      textarea.style.height = `${textarea.scrollHeight}px`; // Đặt chiều cao theo nội dung

      // Giới hạn chiều cao tối đa
      if (textarea.scrollHeight > maxAllowedHeight) {
        textarea.style.overflowY = "auto"; // Hiển thị thanh cuộn khi vượt quá chiều cao tối đa
        textarea.style.height = `${maxAllowedHeight}px`; // Đặt chiều cao tối đa
      } else {
        textarea.style.overflowY = "hidden"; // Ẩn thanh cuộn khi nội dung không vượt quá chiều cao tối đa
        textarea.style.minHeight = `${defaultMinHeight}px`; // Sử dụng chiều cao mặc định khi nội dung không vượt quá
      }
    }
  };

  return (
    <>
      {selectedChat ? (
        <>
          <Box
            fontSize={{ base: "28px", md: "30px" }}
            px={5}
            py={2}
            w="100%"
            fontFamily="Work sans"
            display="flex"
            justifyContent={{ base: "space-between" }}
            alignItems="center"
          >
            <Box display="flex">
              <IconButton
                display={{ base: "flex", md: "none" }}
                mr={5}
                icon={<ArrowBackIcon />}
                onClick={() => setSelectedChat("")}
              />
              <Box>
                <Avatar
                  mr={2}
                  size="md"
                  cursor="pointer"
                  name={
                    !selectedChat.isGroup
                      ? getSender(user, selectedChat.users)
                      : selectedChat.chatName
                  }
                  src={getAvatar(user, selectedChat)}
                />

                {!selectedChat.isGroup ? (
                  <>{getSender(user, selectedChat.users)}</>
                ) : (
                  <>{selectedChat.chatName.toUpperCase()}</>
                )}
              </Box>
            </Box>

            {!selectedChat.isGroup ? (
              <>
                <ProfileModal user={getSenderFull(user, selectedChat.users)} />
              </>
            ) : (
              <>
                <UpdateGroupChatModal
                  fetchMessages={fetchMessages}
                  fetchAgain={fetchAgain}
                  setFetchAgain={setFetchAgain}
                />
              </>
            )}
          </Box>

          <Box
            display="flex"
            flexDir="column"
            justifyContent="flex-end"
            // p={3}
            bg="#E8E8E8"
            w="100%"
            h="100%"
            // borderRadius="lg"
            overflowY="hidden"
          >
            {loading ? (
              <Spinner
                size="xl"
                w={20}
                h={20}
                alignSelf="center"
                margin="auto"
              />
            ) : (
              <div className="messages">
                <ScrollableChat messages={messages} />
              </div>
            )}

            <FormControl
              onKeyDown={sendMessage}
              id="first-name"
              isRequired
              mt={3}
            >
              {istyping ? <div>Loading...</div> : <></>}
              <Box display="flex" alignItems="center" p="3" bg="#fff">
                {/* <Textarea
                  id="myTextArea"
                  variant="unstyled"
                  placeholder="Enter a message..."
                  value={newMessage}
                  onChange={typingHandler}
                  rows={4}
                /> */}

                <Textarea
                  variant="unstyled"
                  placeholder="Enter a message..."
                  value={newMessage}
                  onChange={typingHandler}
                  ref={textareaRef}
                  rows={1}
                  resize="none"
                  maxWidth="100%"
                />

                {/* <Textarea
                  variant="unstyled"
                  placeholder="Enter a message..."
                  value={newMessage}
                  onChange={typingHandler}
                  resize="none" // Cho phép tự động xuống dòng khi nội dung dài hơn
                  rows={1} // Đặt số dòng mặc định là 1
                  maxHeight="200px"
                  minHeight="50px"
                  overflow="auto"
                  maxWidth="100%"
                /> */}

                {/* <Input
                  type="text"
                  placeholder="Enter a message..."
                  value={newMessage}
                  onChange={typingHandler}
                  ref={inputRef}
                  style={{
                    wordWrap: "break-word",
                    overflowWrap: "break-word",
                    // overflow: "hidden", // Ẩn phần nội dung vượt quá chiều cao
                    resize: "none", // Vô hiệu hóa resize của input
                    minHeight: "20px", // Chiều cao tối thiểu (điều chỉnh giá trị tùy ý)
                    maxHeight: "100px", // Giới hạn chiều cao tối đa (điều chỉnh giá trị tùy ý)
                  }}
                  variant="unstyled"
                  // placeholder="Enter a message..."
                  // value={newMessage}
                  // onChange={typingHandler}
                /> */}
                <i className="fas fa-solid fa-paper-plane"></i>
              </Box>
            </FormControl>
          </Box>
        </>
      ) : (
        <Box
          display="flex"
          alignItems="center"
          justifyContent="center"
          h="100%"
        >
          <Text fontSize="3xl" pb={3} fontFamily="Work sans">
            Click on a user to start chatting
          </Text>
        </Box>
      )}
    </>
  );
};

export default SingleChat;
