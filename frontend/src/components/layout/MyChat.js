import { useEffect, useState } from "react";
import axios from "axios";
import { ChatState } from "../../context/ChatProvider";
import {
  Avatar,
  Box,
  Button,
  Stack,
  Text,
  useToast,
  Icon,
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import { getAvatar, getSender } from "../../config/ChatConfig";
import ChatLoading from "./ChatLoading";
import GroupChatModal from "../modal/GroupChatModal";

const MyChat = ({ fetchAgain }) => {
  const [loggedUser, setLoggedUser] = useState();
  const { user, chats, selectedChat, setSelectedChat, setChats } = ChatState();

  const toast = useToast();

  const fetchChats = async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${user.access_token}`,
        },
      };

      const { data } = await axios.get("/api/chat", config);
      setChats(data);
    } catch (error) {
      toast({
        title: "Error Occured!",
        description: "Failed to Load the chats",
        status: "error",
        duration: 5000,
        isClosable: true,
        position: "bottom-left",
      });
    }
  };

  useEffect(() => {
    setLoggedUser(JSON.parse(localStorage.getItem("userInfo")));
    fetchChats();
    if (selectedChat) {
      setSelectedChat(selectedChat);
    }
    // eslint-disable-next-line
  }, [fetchAgain]);

  return (
    <Box
      d={{ base: selectedChat ? "none" : "flex", md: "flex" }}
      flexDir="column"
      alignItems="center"
      // p={2}
      bg="white"
      w={{ base: "100%", md: "20%" }}
      // borderRadius="lg"
      borderWidth="1px"
    >
      {/* <Box
        pb={3}
        px={3}
        fontSize={{ base: "14px", md: "16px", lg: "18px" }}
        fontFamily="Work sans"
        display="flex"
        w="100%"
        justifyContent="space-between"
        alignItems="center"
      >
        My Chats
        <GroupChatModal>
          <Button
            display="flex"
            fontSize={{ base: "14px", md: "12px", lg: "13px", xl: "14px" }}
            rightIcon={<AddIcon />}
          >
            New group
          </Button>
        </GroupChatModal>
      </Box> */}
      <Box
        display="flex"
        flexDir="column"
        // p={3}
        // bg="#F8F8F8"
        w="100%"
        h="100%"
        // borderRadius="lg"
        overflowY="hidden"
      >
        {chats ? (
          <Stack overflowY="scroll" spacing="0">
            {chats.map((chat) => (
              <Box
                display="flex"
                _hover={{
                  background: selectedChat === chat ? "#38B2AC" : "#E8E8E8",
                  // color: "white",
                }}
                onClick={() => setSelectedChat(chat)}
                cursor="pointer"
                bg={selectedChat === chat ? "#38B2AC" : ""}
                color={selectedChat === chat ? "white" : "black"}
                px={3}
                py={2}
                // borderRadius="lg"
                key={chat._id}
              >
                <Avatar
                  mr={2}
                  size="md"
                  cursor="pointer"
                  name={
                    !chat.isGroup
                      ? getSender(loggedUser, chat.users)
                      : chat.chatName
                  }
                  src={getAvatar(loggedUser, chat)}
                />
                {/* <Icon viewBox="0 0 200 200" color="green.400">
                  <path
                    fill="currentColor" 
                    d="M 100, 100 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0"
                  />
                </Icon> */}
                <Box>
                  <Text fontWeight={500}>
                    {!chat.isGroup
                      ? getSender(loggedUser, chat.users)
                      : chat.chatName}
                  </Text>

                  {chat.latestMessage && (
                    <Text fontSize="xs" color="#">
                      <b>{chat.latestMessage.sender.name}: </b>
                      {chat.latestMessage.content.length > 50
                        ? chat.latestMessage.content.substring(0, 51) + "..."
                        : chat.latestMessage.content}
                    </Text>
                  )}
                </Box>
              </Box>
            ))}
          </Stack>
        ) : (
          <ChatLoading />
        )}
      </Box>
    </Box>
  );
};

export default MyChat;
