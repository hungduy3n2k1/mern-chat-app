import { ViewIcon } from "@chakra-ui/icons";
import {
  Button,
  IconButton,
  Image,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
  Box,
} from "@chakra-ui/react";
import { ChatState } from "../../context/ChatProvider";

const UpdateProfile = ({ handleBackToSettingModal }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { user } = ChatState();

  return (
    <>
      {/* {children ? ( */}
      {/* <span onClick={onOpen}>{children}</span> */}
      {/* // ) : ( //{" "}
      <IconButton d={{ base: "flex" }} icon={<ViewIcon />} onClick={onOpen} />
      // )} */}
      <Modal onClose={handleBackToSettingModal} isOpen={true}>
        <ModalOverlay />
        <ModalContent h="410px">
          <ModalHeader
            // fontSize="40px"
            fontFamily="Work sans"
            display="flex"
            // justifyContent="center"
          >
            Info
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody
            display="flex"
            flexDir="column"
            // alignItems="center"
            // justifyContent="space-between"
            p={0}
          >
            <Box display="flex" px={5} py={3} borderBottomWidth="1px">
              <Image
                mr={5}
                borderRadius="full"
                boxSize="80px"
                src={user.avatar}
                alt={user.name}
              />
              <Box>
                <Text as="b" fontSize={{ base: "24px" }} fontFamily="Work sans">
                  {user.name}
                </Text>
                <Text fontSize={{ base: "14px" }} fontFamily="Work sans">
                  {user.email}
                </Text>
              </Box>
            </Box>
            <Box>
              <Button
                w="100%"
                p={0}
                borderRadius={0}
                variant={"ghost"}
                justifyContent="left"
              >
                <Box w="15%">
                  <i className="fas fa-solid fa-users"></i>
                </Box>
                Edit profile
              </Button>
            </Box>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleBackToSettingModal}>Close</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default UpdateProfile;
