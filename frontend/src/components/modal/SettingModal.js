import { ViewIcon } from "@chakra-ui/icons";
import {
  Button,
  IconButton,
  Image,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
  Box,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { ChatState } from "../../context/ChatProvider";
import UpdateProfile from "../modal/UpdateProfile";

const SettingModal = ({ children }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isUpdateProfileOpen, setIsUpdateProfileOpen] = useState(false);

  const { user } = ChatState();

  const openUpdateProfileModal = () => {
    setIsUpdateProfileOpen(true);
    onClose();
  };

  const handleBackToSettingModal = () => {
    setIsUpdateProfileOpen(false);
    onOpen(); // Re-open the setting modal
  };

  return (
    <>
      {children ? (
        <span onClick={onOpen}>{children}</span>
      ) : (
        <IconButton d={{ base: "flex" }} icon={<ViewIcon />} onClick={onOpen} />
      )}

      <Modal onClose={onClose} isOpen={isOpen}>
        <ModalOverlay />
        <ModalContent h="410px">
          <ModalHeader
            // fontSize="40px"
            fontFamily="Work sans"
            display="flex"
            // justifyContent="center"
          >
            Settings
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody
            display="flex"
            flexDir="column"
            // alignItems="center"
            // justifyContent="space-between"
            p={0}
          >
            <Box display="flex" px={5} py={3} borderBottomWidth="1px">
              <Image
                mr={5}
                borderRadius="full"
                boxSize="80px"
                src={user.avatar}
                alt={user.name}
              />
              <Box>
                <Text as="b" fontSize={{ base: "24px" }} fontFamily="Work sans">
                  {user.name}
                </Text>
                <Text fontSize={{ base: "14px" }} fontFamily="Work sans">
                  {user.email}
                </Text>
              </Box>
            </Box>
            <Box>
              {/* <UpdateProfile> */}
              <Button
                w="100%"
                p={0}
                borderRadius={0}
                variant={"ghost"}
                justifyContent="left"
                onClick={openUpdateProfileModal}
              >
                <Box w="15%">
                  <i className="fas fa-solid fa-users"></i>
                </Box>
                Edit profile
              </Button>
              {/* </UpdateProfile> */}
            </Box>
          </ModalBody>
          <ModalFooter>
            <Button onClick={onClose}>Close</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>

      {isUpdateProfileOpen && (
        <UpdateProfile handleBackToSettingModal={handleBackToSettingModal} />
      )}
    </>
  );
};

export default SettingModal;
